/// <reference path="../../../typings/tsd.d.ts" />

namespace Theme.Home {
    "use strict";

    /*@ngInject*/
    /**
     * HomeController
     */
    export class HomeController {

        constructor(
            private $state: ng.ui.IStateService,
            $q: ng.IQService
        ) {

        }
    }
}
