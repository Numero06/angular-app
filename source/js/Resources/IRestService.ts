/// <reference path="_Resources.ts" />

namespace Theme.Resources {

    /**
     * IRestService
     */
    export interface IRestService {
        /**
         *
         */
        get(restUrl: string, opts?: Object): Classes.IPromise<any>;

        /**
         *
         */
        get<T>(restUrl: string, opts?: Object): Classes.IPromise<T>;

        /**
         *
         */
        post(restUrl: string, data?: any, opts?: Object): Classes.IPromise<any>;

        /**
         *
         */
        post<T>(restUrl: string, data?: any, opts?: Object): Classes.IPromise<T>;

        /**
         *
         */
        put(restUrl: string, data?: any, opts?: Object): Classes.IPromise<any>;

        /**
         *
         */
        put<T>(restUrl: string, data?: any, opts?: Object): Classes.IPromise<T>;
    }

}
