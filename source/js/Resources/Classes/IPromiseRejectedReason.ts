/// <reference path="_Classes.ts" />

namespace Theme.Resources.Classes {

    /**
     *
     */
    export interface IPromiseRejectedReason {
        /**
         * reason
         */
        data: string;
        /**
         * 
         */
        message: string;
    }

}
