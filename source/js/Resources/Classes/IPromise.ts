/// <reference path="_Classes.ts" />

namespace Theme.Resources.Classes {

    /**
     *
     */
    export interface IPromise<T> extends ng.IPromise<T> {

        then<TResult>(successCallback: (promiseValue: T) => IPromise<TResult>|TResult, errorCallback?: (reason: IPromiseRejectedReason) => any, notifyCallback?: (state: any) => any): IPromise<TResult>;

        catch<TResult>(onRejected: (reason: IPromiseRejectedReason) => IPromise<TResult>|TResult): IPromise<TResult>;

        finally(finallyCallback: () => any): IPromise<T>;
    }

}
