/// <reference path="IResponseSort.ts" />

namespace Theme.Resources.Classes {

    /**
     *
     */
    export interface IResponseBase<TContentObject> {
        /**
         *
         */
        content: Array<TContentObject>;

        /**
         * 
         */
        items: Array<TContentObject>;

        /**
         *
         */
        totalPages: number;

        /**
         *
         */
        totalElements: number;

        /**
         *
         */
        last: boolean;

        /**
         *
         */
        size: number;

        /**
         *
         */
        number: number;

        /**
         *
         */
        sort: Array<IResponseSort>;

        /**
         *
         */
        first: boolean;

        /**
         *
         */
        numberOfElements: number;
    }

}
