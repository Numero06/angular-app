namespace Theme.Resources.Classes {

    /**
     *
     */
    export interface IResponseSort {
        /**
         *
         */
        direction: "DEST" | "ASC";

        /**
         *
         */
        property: string;

        /**
         *
         */
        ignoreCase: boolean;

        /**
         *
         */
        nullHandling: string;

        /**
         *
         */
        ascending: boolean;
    }

}
