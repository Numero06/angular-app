/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="IRestService.ts" />

namespace Theme.Resources {

    /**
     * RestService
     */
    export class RestService implements IRestService {

        constructor(private $q: ng.IQService, private $http: ng.IHttpService) {
        }

        /**
         * authenticate
         */
        public get(restUrl: string, opts: Object = {}) {
            let request: any = opts;

            request.method = "GET";
            request.url = restUrl;

            return this.makeRequest(request);
        }

        /**
         * post
         */
        public post(restUrl: string, data: any = {}, opts: Object = {}) {
            let request: any = opts;

            request.method = "POST";
            request.url = restUrl;
            request.data = data;

            return this.makeRequest(request);
        }

        public put(restUrl: string, data: any = {}, opts: Object = {}) {
            let request: any = opts;

            request.method = "PUT";
            request.url = restUrl;
            request.data = data;

            return this.makeRequest(request);
        }

        private makeRequest(request) {
            let defer = this.$q.defer();

            angular.extend(request, this.getCommonRequestConfig());

            let req = this.$http(request);

            // req.then((response) => {
            //     console.info(response);
            // });

            req.success((data, status, headers) => {
                    defer.resolve(data);
                })
                .error((error) => {
                    defer.reject(error);
                });

            return defer.promise;
        }

        private getCommonRequestConfig() {
            return {};
        }
    }

}
