/// <reference path="../../typings/tsd.d.ts" />

/**
 *
 */
namespace Theme {

    angular
        .module("Theme", [
            "ngAria",
            "ngCookies",
            "ngMessages",
            "ngResource",
            "ngSanitize",
            "ngTouch",
            "LocalStorageModule", // angular-local-storage
            "ui.router",
            "uiSwitch",
            "Theme.Templates",
            "Theme.Constants",
            "swipe"
        ]);

}
