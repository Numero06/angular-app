/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="app.ts" />

namespace Theme {

    /*@ngInject*/
    export function RoutesConfig(
        $stateProvider: ng.ui.IStateProvider,
        $urlRouterProvider: ng.ui.IUrlRouterProvider,
        $urlMatcherFactoryProvider: ng.ui.IUrlMatcherFactory
    ) {

        $stateProvider
            .state("all", {
                abstract: true,
                /*views: {
                    "main@": {
                        templateUrl: "Navbar/navbar.partial.html",
                        controller: "NavbarController",
                        controllerAs: "vm",
                    },
                },*/
            })

            .state({
                name: "home",
                parent: "all",
                url: "/",
                views: {
                    "main@": {
                        templateUrl: "Home/home.partial.html",
                        controller: "HomeController",
                        controllerAs: "vm",
                    },
                },
            });

        $urlMatcherFactoryProvider.defaultSquashPolicy("nosquash");
        $urlRouterProvider.otherwise("/");
    }

}
