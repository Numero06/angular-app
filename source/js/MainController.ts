/// <reference path="app.ts" />

namespace Theme {
    "use strict";

    /*@ngInject*/
    /**
     * MainController
     */
    export class MainController {

        constructor(
            private $rootScope: ng.IRootScopeService,
            private $state: ng.ui.IStateService,
            public ThemeConstants
        ) {}
    }
}
