/// <reference path="../../typings/tsd.d.ts" />
/* global angular */

(function () {
    "use strict";

    function Constants() {
        return {
            /**
             * Contact email
             */
            "ContactEmail": "",
            /**
             * Contact mobile phone number
             */
            "ContactMobile": ""
        };
    }

    angular
        .module("Theme.Constants", [])
        .factory("ThemeConstants", Constants)

})();
