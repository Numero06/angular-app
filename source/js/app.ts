/// <reference path="app.routes.config.ts" />
/// <reference path="app.module.ts" />
/// <reference path="MainController.ts" />
/// <reference path="Home/HomeController.ts" />
/// <reference path="Services/Service.ts" />
/// <reference path="Error/_Error.ts" />
/// <reference path="Resources/_Resources.ts" />


/**
 * App Bootstrapper
 */
namespace Theme {

    angular
        .module("Theme")

        //Services
        .service("rest", Resources.RestService)

        // Controllers
        .controller("MainController", MainController)
        .controller("HomeController", Home.HomeController)
        .controller("ErrorController", Error.ErrorController)

        // Services
        .service("Service", Service.ThemeService)

        // Configs
        .config(["$locationProvider", function ($locationProvider: ng.ILocationProvider) {
            $locationProvider.hashPrefix("!");
        }])

        .config(RoutesConfig)

        // Runs
        .run(/*@ngInject*/($state: ng.ui.IStateService, $rootScope: ng.IRootScopeService) => {

            /*$rootScope.$on("$stateChangeStart", (event, toState, toParams, fromState, fromParams, options) => {
            });*/
        });
}
