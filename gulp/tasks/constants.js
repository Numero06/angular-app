var gulp = require("gulp");
var rename = require("gulp-rename");
var config = require("../tasks.config.js").constantsConfig;

gulp.task("constants", function() {
    "use strict";

    return gulp.src(config.srcFile)
        .pipe(rename(config.destFile))
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
