var gulp = require("gulp"),
    sass = require("gulp-sass"),
    sourcemaps = require("gulp-sourcemaps"),
    config = require("../tasks.config").sass;

gulp.task("sass", function() {
    "use strict";

    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
