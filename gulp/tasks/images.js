var gulp = require("gulp"),
    images = require("../tasks.config.js").images,
    imagemin = require("gulp-imagemin");

gulp.task("images", function () {
    "use strict";

    return gulp.src(images.src)
        .pipe(imagemin())
        .pipe(gulp.dest(images.dest))
        .pipe(gulp.dest(images.testDest));
});