var gulp = require("gulp");
var config = require("../tasks.config").uglifyTsConfig;
var tsc = require("gulp-typescript");
var ngAnnotate = require("gulp-ng-annotate");
var concat = require("gulp-concat");
var sourcemaps = require("gulp-sourcemaps");

gulp.task("uglifyTs", [], function() {
    "use strict";

    gulp.src("theme/js/**/*.ts")
        .pipe(gulp.dest("build/source"))
        .pipe(gulp.dest("test/source"));

    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(tsc(config.tsConfig))
        .js
        .pipe(concat("app.js"))
        .pipe(ngAnnotate())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
