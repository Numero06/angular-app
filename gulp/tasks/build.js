var gulp = require("gulp"),
    zip = require("gulp-zip"),
    rename = require("gulp-rename"),
    config = require("../tasks.config.js");

gulp.task("build", ["uglifyTs", "partials", "bowerComponents", "staticFiles", "images", "less", "sass", "js", "constants", "fonts"], function () {
    "use strict";

    return gulp.src(config.dest + "**/*")
        .pipe(zip("build.zip"))
        .pipe(gulp.dest(config.buildsDest))
        .pipe(rename("build." + config.version + ".zip"))
        .pipe(gulp.dest(config.buildsDest));
});