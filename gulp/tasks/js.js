var gulp = require("gulp");
var config = require("../tasks.config").js;

gulp.task("js", [], function() {
    "use strict";

    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
