var gulp = require("gulp"),
    less = require("gulp-less"),
    sourcemaps = require("gulp-sourcemaps"),
    config = require("../tasks.config").less,
    autoprefixer = require("gulp-autoprefixer");

gulp.task("less", function() {
    "use strict";

    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer({ browsers: ["last 2 version"] }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
