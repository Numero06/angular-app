var gulp = require("gulp");
var config = require("../tasks.config.js").fonts;

gulp.task("fonts", function () {
    "use strict";

    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
