var gulp = require("gulp");
var config = require("../tasks.config.js").bowerComponents;

gulp.task("bowerComponents", function () {
    "use strict";

    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
});
