var config = require("../tasks.config").partials;
var gulp = require("gulp");
// var debug = require("gulp-debug");
var templateCache = require("gulp-angular-templatecache");
// var browserSync = require("browser-sync");

// Views task
gulp.task("partials", function() {
    "use strict";

    return gulp.src(config.src)
        // .pipe(debug({ title: "VIEW:" }))
        .pipe(templateCache(config.templateCache))
        .pipe(gulp.dest(config.dest))
        .pipe(gulp.dest(config.testDest));
    // .pipe(browserSync.reload({ stream: true }));
});
