var gulp = require("gulp"),
    versionNumber = require("gulp-version-number");
var config = require("../tasks.config.js"),
    configStaticFiles = config.staticFiles;

gulp.task("staticFiles", function () {
    "use strict";

    var versionOpts = {
        value: config.version,
        append: {
            key: "_v",
            to: "all"
        },
        "output": {
            "file": configStaticFiles.dest + "version.json"
        }
    };

    return gulp.src(configStaticFiles.src, { base: config.base })
        .pipe(versionNumber(versionOpts))
        .pipe(gulp.dest(configStaticFiles.dest))
        .pipe(gulp.dest(configStaticFiles.testDest));
});
