var gulp = require("gulp");
var del = require("del");
var config = require("../tasks.config");

gulp.task("clean", function () {
    "use strict";

    return del.sync([config.dest, config.testDest]);
});
