var path = require("path"),
    moment = require("moment"),
    argv = require("yargs")
        .usage("Usage: $0 --version='output version'")
        .string("V")
        .alias("V", "version")
        .default("version", moment().format("YYYYMMDDHHmm"))
        .help("help")
        .argv;

var dest = "build/";
var buildsDest = "builds/";
var src = "source/";
var testDest = "test/";

module.exports = {
    base: src,
    dest: dest,
    buildsDest: buildsDest,
    testDest: testDest,
    version: argv.version,

    uglifyTsConfig: {
        src: src + "**/app.ts",
        dest: dest + "js/",
        testDest: testDest + "js/",
        tsConfig: {
            outFile: "app.js",
            target: "ES5"
        }
    },

    partials: {
        src: src + "**/*.partial.html",
        dest: dest + "js/",
        testDest: testDest + "js/",
        templateCache: {
            standalone: true,
            filename: "app.partials.js",
            module: "Theme.Templates",
            transformUrl: function (url) {
                "use strict";

                var dir = path.dirname(url).replace(/^js[\/\\]/, ""),
                    file = path.basename(url); // .replace(/.partial/, "");

                return dir + "/" + file;
            }
        }
    },

    bowerComponents: {
        src: [src + "bower_components/**", "!" + src + "bower_components/**/*.exe"],
        dest: dest + "bower_components/",
        testDest: testDest + "bower_components/"
    },

    staticFiles: {
        src: [src + "index.html", src + "files/**"],
        dest: dest,
        testDest: testDest
    },

    fonts: {
        src: [src + "**/*.{ttf,woff,eof,otf,woff2,svg,eot}"],
        dest: dest,
        testDest: testDest
    },

    images: {
        src: ["**/*.svg", "**/*.png", "**/*.ico", "**/*.jpg", "**/*.js"].map(function (path) { "use strict"; return src + path; }),
        dest: dest,
        testDest: testDest,
    },

    less: {
        src: src + "css/main.less",
        dest: dest + "css",
        testDest: testDest + "css",
    },

    sass: {
        src: src + "css/main.scss",
        dest: dest + "css",
        testDest: testDest + "css",
    },

    js: {
        src: [src + "js/**/*.js", "!" + src + "js/**/*constants.js"],
        dest: dest + "js",
        testDest: testDest + "js",
    },

    constantsConfig: {
        srcFile: src + "js/app.constants.js",
        destFile: "app.constants.js",
        dest: dest + "js",
        testDest: testDest + "js",
    }
};
